<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Main extends Model
{
    use HasFactory;
    protected $table = 'data_vis';
    protected $fillable = ['co_email'];

    public static function boot()
    {
        parent::boot();
        self::creating(function($model){
            $max = 0;
            $model->rsvp_logs = self::recrusionSlug($max);
        });
    }

    private static function recrusionSlug($max){
        $slug = Str::random(5);
        $find = self::where('rsvp_logs',$slug)->first();
        if($max>3){
            abort('500','Code:Lx00024(M5)');
        }
        if($find){
            $max++;
            self::recrusionSlug($max);
        }
        return $slug;
    }

}
