@extends('layouts.email')

@section('content')
    <table>
        <tr>
            <td colspan="2">
                <img src="https://virtualcareerexpo.1id.my/images/banner.png" width="100%">
            </td>
        </tr>
        <tr><td colspan="2">
            <p>Hi {{ $model['per_designation'] }} {{ $model['per_firstname'] }} {{ $model['per_lastname'] }} ,</p>
            <p>Thank you for registering for "Virtual Career Expo Business Events Edition 2020".</p>
        </td></tr>
        <tr><td style="white-space: nowrap">Date</td><td width="100%">: 9th Nov 2020</td></tr>
        <tr><td style="white-space: nowrap">Registration Starts</td><td>: 11.00am</td></tr>
        <tr><td style="white-space: nowrap">Event Starts</td><td>: 11.30am – 2:30pm</td></tr>
        <tr><td style="white-space: nowrap">Venue</td><td>: Ballroom, Courtyard by Marriott Penang</td></tr>
        <tr><td colspan="2">
                <p>We look forward to your attendance. For any additional enquires, do feel free to drop us an email.</p>
                <p>Please submit any questions to: hello@virtualcareerexpo.my</p>
                <p><img style="background-color:white;padding:10px;width:170px" src="http://app.1id.my/site/qr-generator2?params={{ $model['rsvp_logs'] }}"></p>
                <p>Thank you.</p>
        </td></tr>
    </table>

@endsection
