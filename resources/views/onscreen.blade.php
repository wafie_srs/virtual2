@extends('layouts.master')

@section('content')
    <div class="flash-message mt-2">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    @endif
    <table>
        <tr>
            <td colspan="2">
                <img src="https://virtualcareerexpo.1id.my/images/banner.png" width="100%">
            </td>
        </tr>
        <tr><td colspan="2"><p>Launching of Virtual Career Expo Business Events Edition 2020</p></td></tr>
        <tr><td colspan="2"><p>Thank you for registering!.</p></td></tr>
        <tr><td style="white-space: nowrap">Name</td><td width="100%">: {{ $model['per_designation'] }} {{ $model['per_firstname'] }} {{ $model['per_lastname'] }}</td></tr>
        <tr><td style="white-space: nowrap">Company</td><td width="100%">: {{ $model['co_name'] }}</td></tr>
        <tr><td style="white-space: nowrap">Date</td><td width="100%">: 9th Nov 2020</td></tr>
        <tr><td style="white-space: nowrap">Registration Starts</td><td>: 11.00am</td></tr>
        <tr><td style="white-space: nowrap">Event Starts</td><td>: 11.30am – 2:30pm</td></tr>
        <tr><td style="white-space: nowrap">Venue</td><td>: Ballroom, Courtyard by Marriott Penang</td></tr>
        <tr><td colspan="2">
                <p>Please arrive earlier to check in as the event practices strict SOP.</p>
                <p>
                    <b>HEALTH DECLARATION</b><br>
                    To prevent the spread of COVID-19 and reduce the risk of exposure, please click <a href="https://docs.google.com/forms/d/e/1FAIpQLSegN1wMJuIHzz8WD75g--SEMTfHtE2WjUsp9atRaSBISk_zEg/viewform?usp=sf_link">here</a> to declare your health condition.
                </p>
                <p>
                    <b>SOCIAL DISTANCING REMINDER</b><br>
                    Please remember to keep a distance of at least 1 metre between yourself and others. Wearing a mask is mandatory throughout the whole event. Your cooperation is an important precautionary measure to everyone!
                </p>
                <p>
                    <b>IMPORTANT</b><br>
                    You will receive a confirmation email on your registration shortly, if you don't find it in your inbox, kindly check your spam folder.<br>
                    Please screenshot the following <b>unique QR code</b>, save in your phone and present it during registration on the event day. Please remember to wear a face mask, thank you and stay safe.
                </p>
                <p class="text-center"><img style="background-color:white;padding:10px;width:170px" src="http://app.1id.my/site/qr-generator2?params={{ $model['rsvp_logs'] }}"></p>
            </td></tr>
    </table>

@endsection
