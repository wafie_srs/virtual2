@extends('layouts.master')

@section('content')
    <div class="card bg-white shadow mt-3">
        <div class="card-body">
            <img class="mb-2" src="{{ asset('images/banner.png') }}" width="100%">
            <div class="flash-message mt-2">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            <form method="post">
                @csrf
                <input name="co_email" class="form-control" placeholder="Email ID*" required  value="{{ old('co_email') }}" type="email">
                <small for="co_email" class="form-text mb-3"></small>
                <div class="form-inline">
                    <select name="per_designation" class="form-control mr-md-3 mb-3" required>
                        <option @if (old('per_designation') == "") {{ 'selected' }} @endif value="">-Select-</option>
                        <option @if (old('per_designation') == "Mr.") {{ 'selected' }} @endif value="Mr.">Mr.</option>
                        <option @if (old('per_designation') == "Ms.") {{ 'selected' }} @endif value="Mrs.">Ms.</option>
                    </select>
                    <input name="per_firstname" placeholder="First Name*"  class="form-control mr-md-3 mb-3"  required value="{{ old('per_firstname') }}" >
                    <input name="per_lastname" placeholder="Last Name" class="form-control mb-3" value="{{ old('per_lastname') }}" >
                </div>
                <input name="co_jobtitle" class="form-control mb-3" placeholder="Job Title/ Position*"  required  value="{{ old('co_jobtitle') }}">
                <input name="co_name" class="form-control mb-3" placeholder="Organisation*" required  value="{{ old('co_name') }}">
                <input type="tel" id="co_mobile" name="co_mobile" class="form-control mb-3" placeholder="Mobile"  required  value="{{ old('co_mobile') }}">
                <div class="text-center">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
<script>
$('input[required],select[required]').each(function(){
    $(this).on('keyup focusout',function(){
        $.ajax({
            type: "post",
            url: "{{ route('ajax') }}",
            data: { column:$(this).attr('name'),value: $(this).val(), _token: "{{ csrf_token() }}"},
            dataType: 'json',              // let's set the expected response format
            success: function(data){
                $('input[name="'+data.column+'"]').removeClass('is-invalid').addClass('is-valid');
                $('select[name="'+data.column+'"]').removeClass('is-invalid').addClass('is-valid');
                $('[for="'+data.column+'"]').html('').removeClass('invalid-feedback');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('input[name="'+key+'"]').addClass('is-invalid').removeClass('is-valid');
                    $('select[name="'+key+'"]').addClass('is-invalid').removeClass('is-valid');
                    $('[for="'+key+'"]').html(value).addClass('invalid-feedback');
                });
            }
        });
    })
})

if($('.alert-danger').length){
    $('form').addClass('was-validated');
    var email = $('input[name="co_email"]');
    $.ajax({
        type: "post",
        url: "{{ route('ajax') }}",
        data: { column:email.attr('name'),value: email.val(), _token: "{{ csrf_token() }}"},
        dataType: 'json',              // let's set the expected response format
        success: function(data){
            $('input[name="'+data.column+'"]').removeClass('is-invalid').addClass('is-valid');
            $('select[name="'+data.column+'"]').removeClass('is-invalid').addClass('is-valid');
            $('[for="'+data.column+'"]').html('').removeClass('invalid-feedback');
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('input[name="'+key+'"]').addClass('is-invalid').removeClass('is-valid');
                $('select[name="'+key+'"]').addClass('is-invalid').removeClass('is-valid');
                $('[for="'+key+'"]').html(value).addClass('invalid-feedback');
            });
        }
    });
}

$('button[type=submit]').on('click',function(e){
    if($('.is-invalid').length){
        e.preventDefault();
        return false;
    }
    return true;
})
</script>
<script src={{ asset("telinput/build/js/intlTelInput.js") }}></script>
<script>
var input = $('#co_mobile');
var iti =  window.intlTelInput(input.get(0), {
    preferredCountries:['MY','SG'],
    separateDialCode:false,
});
if(input.val()==''){
    input.val('+60');
}

input.keyup(function() {
    var text = '+'+iti.getSelectedCountryData().dialCode;
    if(text=="+undefined"){
        $(this).val('+60');
    } else {
        $(this).val(text + this.value.replace(text,""));
    }
});

</script>

@endsection
